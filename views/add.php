<table class="layout1" id="edition">
	<tbody class="edition_table">
		<tr class="line">
			<td class="center">
				<p>Choose the type of node you want to add:</p>
			</td>
		</tr>
		<tr class="flowing">
			<td class="icons_view">
				<?php foreach($board->node()->get_allowed_child_types() as $type): ?>
					<?php $name=ucfirst($board->config()->get_localized_type($type)) ?>
					<div class="node">
						<a href="<?php echo $board->node()->link(array('add'=>$type)) ?>">
							<?php echo $board->generate_thumbnail($board->config()->get_thumb($type), ICON_BIG, $name) ?>
							<?php echo $name ?>
						</a>
					</div>
				<?php endforeach ?>
			</td>
		</tr>

</table>

