<form action="<?php echo $board->node()->link('delete') ?>" method="post" class="layout1">
	<table class="layout1">
		<tbody>
			<tr>
				<td id="edition">
					<table class="edition_table">
						<tbody>
							<tr class="line">
								<td>
									<p>Are you sure you want to delete this node ? The following nodes will be deleted as well:</p>
								</td>
							</tr>
							<tr class="flowing">
								<td class="tree_view">
									<?php echo $board->node()->button(ICON_TINY) ?>
									<?php echo $board->list_all_childs($board->node()) ?>
								</td>
							</tr>
							<tr class="line">
								<td class="center">
									<a href="<?php echo $board->node()->link() ?>" class="button"><?php echo $board->config()->localize('Cancel') ?></a>
									<input class="button" type="submit" name="delete" value="<?php echo $board->config()->localize('Submit') ?>" />
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td id="aside">
					<?php include PROJECT_REAL_PATH.'includes/edit_aside.php' ?>
				</td>
			</tr>
		</tbody>
	</table>
</form>