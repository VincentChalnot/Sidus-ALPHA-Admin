<?php if($board->node()->get_auth('edit')): ?>
<?php $form=$board->node()->form('permissions'); ?>
<table class="layout1"><tbody><tr><td id="edition">
	<form action="<?php echo $board->node()->link(array('edit'=>'permissions')) ?>" method="post">
	<table class="edition_table permissions"><tbody>
		<tr class="line"><td colspan="8"><?php $form->display('inherit_permissions') ?></td></tr>
		<?php if(!$board->node()->get('inherit_permissions')){ ?>
		<tr class="line">
			<td><?php $board->localize('User').' / '.$board->localize('Group') ?></td>
			<td><?php echo $board->localize('Read') ?></td>
			<td><?php echo $board->localize('Add') ?></td>
			<td><?php echo $board->localize('Edit') ?></td>
			<td><?php echo $board->localize('Delete') ?></td>
			<td><?php echo $board->localize('Ownership') ?></td>
			<td><?php echo $board->localize('Mastership') ?></td>
			<td><?php echo $board->localize('Inverse') ?></td>
			<td>&nbsp;</td>
		</tr>
		<tr class="line box">
			<td><?php echo $board->localize('Anonymous') ?></td>
			<td><?php $form->display('anonymous.read') ?></td>
			<td><?php $form->display('anonymous.add') ?></td>
			<td><?php $form->display('anonymous.edit') ?></td>
			<td><?php $form->display('anonymous.delete') ?></td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<?php
		foreach($board->node()->get_permissions() as $entity){ ?>
			<tr class="line box active">
				<td><?php echo $board->node($entity['entity_id']) ?></td>
				<?php
				foreach(array('read','add','edit','delete','ownership','mastership','inverse') as $perm){
					echo '<td>';
					$form->display('entity_'.$entity['entity_id'].'.'.$perm);
					echo '</td>';
				}
				?>
				<td><a href="<?php echo $board->node()->link(array('edit'=>'permissions', 'remove'=>$entity['entity_id'])) ?>" class="button"><?php echo $board->localize('Remove') ?></a></td>
			</tr><?php
		}
		if($form->get('new_entity_id')!=false){
		?>
			<tr class="line box">
				<td><?php $form->display('new_entity_id') ?></td>
				<?php
				foreach(array('read','add','edit','delete','ownership','mastership','inverse') as $perm){
					echo '<td>';
					$form->display('new.'.$perm);
					echo '</td>';
				}
				?>
				<td><input class="button" type="submit" name="add_new_entity" value="<?php echo $board->localize('Add') ?>" /></td>
			</tr>
		<?php }
		}?>
		<tr class="flowing"></tr>
		<tr class="line"><td colspan="3">&nbsp;</td><td><input class="button" type="submit" name="save" value="<?php echo $board->localize('Save') ?>" /></td><td>&nbsp;</td><td><input class="button" type="submit" name="submit" value="<?php echo $board->localize('Submit') ?>" /></td><td colspan="3">&nbsp;</td></tr>
	</tbody></table>
	</form>
</td><td id="aside">
<?php include PROJECT_REAL_PATH.'includes/edit_aside.php' ?>
</td></tr></tbody></table>
<?php include PROJECT_REAL_PATH.'includes/tiny_mce.php' ?>
<?php endif ?>