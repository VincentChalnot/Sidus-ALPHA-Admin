<?php if(!isset($board)){trigger_error('You must set $board before loading the following view :'.$_SERVER['SCRIPT_NAME']);return;} ?>
<table class="layout1">
	<tbody>
		<tr>
			<td id="edition">
				<?php echo $board->form()->form_tag() ?>
					<table class="edition_table">
						<tbody>
							<tr class="line">
								<td class="center">
									<h2><?php echo $board->form()->get_title() ?></h2>
								</td>
							</tr>
							<?php foreach($board->form()->get_all() as $input): ?>
								<?php if($input->get_type()!='hidden'): ?>
									<tr class="<?php echo $input->get_name()=='content'?'flowing':'line' ?>">
										<td>
											<?php echo $input->display() ?>
										</td>
									</tr>
								<?php else: ?>
									<?php echo $input->display() ?>
								<?php endif ?>
							<?php endforeach ?>
							<tr class="line">
								<td class="center">
									<input class="button" type="submit" name="submit" value="<?php echo $board->localize('Submit') ?>" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</td>
			<td id="aside">
				<?php include PROJECT_REAL_PATH.'includes/add_aside.php' ?>
			</td>
		</tr>
	</tbody>
</table>
<?php
include PROJECT_REAL_PATH.'includes/tiny_mce.php';
