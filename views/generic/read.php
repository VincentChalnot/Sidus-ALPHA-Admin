<table class="layout1">
	<tbody>
		<tr>
			<?php include PROJECT_REAL_PATH.'includes/siblings_aside.php' ?>
			<td id="content">
				<article><?php echo $board->node()->get_html_content() ?></article>
				<?php $board->adaptable_view($board->node()->get_childs('index_num', 'ASC', null, array('-icon', '-revision', '-menu', '-resources'))) ?>
				<hr class="clear"/>
				<div class="keywords"><?php echo $board->generate_tags_links($board->node()) ?></div>
				<?php $board->node()->add_view() ?>
			</td>
			<?php include PROJECT_REAL_PATH.'includes/aside.php' ?>
		</tr>
	</tbody>
</table>