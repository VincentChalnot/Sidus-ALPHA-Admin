<?php $children = $board->node()->get_childs('index_num', 'ASC', null, array('icon', 'revision', 'menu', 'resources')) ?>
<?php if(!$board->node()->is_root() || ($children && count($children) > 0)): ?>
	<td id="treeview">
		<?php if(!$board->node()->is_root()): ?>
			<p class="section"><?php echo $board->localize('Siblings list') ?></p>
			<?php $siblings = $board->node()->get_parent()->get_childs() ?>
			<?php if($siblings): ?>
				<?php foreach($siblings as $sibling): ?>
					<?php if($sibling->get_auth('read')): ?>
						<?php if($sibling->get('node_id') == $board->node()->get('node_id')): ?>
							<?php echo $sibling->button(ICON_SMALL, '', array('class'=>'active')) ?>
						<?php else: ?>
							<?php echo $sibling->button() ?>
						<?php endif ?>
					<?php endif ?>
				<?php endforeach ?>
			<?php endif ?>
		<?php endif ?>

		<?php if($children && count($children) > 0): ?>
			<p class="section"><?php echo $board->localize('Hidden children') ?></p>
			<?php foreach($children as $child): ?>
				<?php if($child->get_auth('read')): ?>
					<?php echo $child->button() ?>
				<?php endif ?>
			<?php endforeach ?>
		<?php endif ?>
	</td>
<?php endif ?>