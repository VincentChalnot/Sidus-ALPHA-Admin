<script type="text/javascript" src="<?php echo PROJECT_HTTP_PATH.'scripts/tinymce/jquery.tinymce.min.js' ?>"></script>
<script type="text/javascript">
	$().ready(function() {
		//return;
		$('textarea').tinymce({
			// Location of TinyMCE script
			script_url: '<?php echo PROJECT_HTTP_PATH ?>scripts/tinymce/tinymce.min.js',
			selector: "textarea",
			theme: "modern",
			plugins: [
					"advlist autolink lists link image charmap print preview hr anchor pagebreak",
					"searchreplace wordcount visualblocks visualchars code fullscreen",
					"insertdatetime media nonbreaking save table contextmenu directionality",
					"emoticons template paste textcolor"
			],
			toolbar1: "insertfile undo redo | styleselect fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print media | forecolor backcolor emoticons",
			image_advtab: true,
			templates: [
				{title: 'Test template 1', content: 'Test 1'},
				{title: 'Test template 2', content: 'Test 2'}
			],
			theme_advanced_resizing: false,
			extended_valid_elements: "iframe[src|width|height|name|align]",
			content_css : "<?php echo PROJECT_HTTP_PATH ?>../css/common.css"
		});
	});
</script>