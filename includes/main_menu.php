<form method="get" action="<?php echo $board->node()->link() ?>">
	<label for="search" class="search">
		<input id="search" type="text" name="search" placeholder="<?php echo $board->localize('Search') ?>" value="<?php echo isset($_GET['search']) ? $board->secure_display($_GET['search']) : '' ?>"/>
		<input type="image" src="<?php echo HTTP_PATH.'images/actions/search.png'; ?>" alt="<?php echo $board->localize('Search') ?>" />
		<input type="submit" value="" />
	</label>
</form>
<div class="tools_tab">
	<?php if($board->user()->is_connected()): ?>
		<a href="<?php echo $board->user()->link() ?>"><?php echo $board->user()->get_html_thumb(ICON_SMALL) ?> <span><?php echo $board->user() ?></span></a>
		<a href="<?php echo $board->node()->link('config') ?>"><?php echo $board->generate_icon('config',25) ?></a>
	<?php else: ?>
		<a href="<?php echo $board->node()->link('login') ?>" title="<?php echo $board->localize('Login') ?>"><?php echo $board->generate_icon('secure',25) ?> <span><?php echo $board->localize('Login') ?></span></a>
	<?php endif ?>
</div>