<?php
$childs=$board->node()->get_childs('creation','ASC',null,array('comment'));
foreach($childs as $child){
  if($child->get_auth('read')){ ?>
		<div class="message" id="message_<?php echo $child->get('node_id')?>">
			<div class="buttons">
			<?php
			if($child->get_auth('delete') || $child->get_auth('edit'))
				echo '<a href="'.$child->link().'" class="go" title="Goto"></a>';
			?>
			</div>
			<div class="avatar"><?php echo $child->get_owner->get_thumb_html(ICON_NORMAL); ?></div>
			<div class="content">
				<?php
				echo '<div><b>'.$child->get('title').'</b> ; ';
				if($child->get_owner()!=false){
					echo '<a href="'.$child->get_owner()->link().'" class="user_link">'.$child->get_owner()->get('username').'</a> ';
				}
				echo $board->config->localize('the').' <em class="time" title="'.$child->get_date('creation').'">'.$child->get_date('creation',DATE_TINY).'</em></div>';
				echo $child->get('content');
				?><br/>
			</div>
		</div>
<?php }
} ?>