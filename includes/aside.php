<td id="aside">
	<div class="preview">
		<?php echo $board->node()->get_html_thumb(ICON_HUGE) ?>
	</div>

	<?php $owners=$board->node()->get_owners() ?>
	<?php if(count($owners)>0 && $owners!=false): ?>
		<p class="section">
			<?php echo $board->localize((count($owners)>1)?'Owners':'Owner') ?>
		</p>
		<?php foreach($owners as $owner): ?>
			<?php echo $owner->button() ?>
		<?php endforeach ?>
	<?php endif ?>

	<?php if($board->node()->get_auth('add') || $board->node()->get_auth('edit') || $board->node()->get_auth('delete')): ?>
		<p class="section"><?php echo $board->localize('Actions') ?></p>
		<?php if($board->node()->get_auth('add')) echo $board->node()->action_button('add') ?>
		<?php if($board->node()->get_auth('edit')) echo $board->node()->action_button('edit') ?>
		<?php if($board->node()->get_auth('delete')) echo $board->node()->action_button('delete') ?>
	<?php endif ?>

	<p class="section"><?php echo $board->localize('Tools') ?><p>
	<?php echo $board->generate_button($board->project_link($board->node()), 'read', $board->localize('Preview')) ?>
	<?php echo $board->node()->action_button('print') ?>
	<?php echo $board->node()->action_button('feed') ?>
	<?php //echo $board->node()->action_button('share') ?>


	<?php if(method_exists($board->node(), 'get_original')): ?>
		<?php $original=$board->node()->get_original() ?>
		<?php if($original): ?>
			<?php echo $board->node()->action_button('download') ?>
		<?php endif ?>
	<?php endif ?>
</td>