<p class="section"><?php echo $board->localize('Types') ?></p>
<?php foreach($board->node()->get_allowed_child_types() as $type): ?>
	<?php $name=ucfirst($board->config()->get_localized_type($type)) ?>
	<a href="<?php echo $board->node()->link(array('add'=>$type)) ?>" class="button<?php echo $board->form()->get('type_name')->get_value()==$type?' active':'' ?>">
		<?php echo $board->generate_thumbnail($board->config()->get_thumb($type), ICON_SMALL, $name) ?>
		<?php echo $name ?>
	</a>
<?php endforeach ?>