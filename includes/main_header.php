<div class="location">
	<?php foreach(array_reverse($board->node()->get_parents()) as $parent): ?>
		<?php echo $parent->button() ?>
	<?php endforeach ?>
	<?php echo $board->node()->button(ICON_SMALL, '', array('class'=>'active')) ?>
</div>
<?php if($board->node()->get_auth('read')): ?>
	<div class="side">
		<?php echo ucwords($board->config()->get_localized_type($board->node()->get('type_name'))) ?>
		<?php echo $board->localize('created the') ?>
		<em class="time" title="<?php echo $board->localize('the').' '.$board->node()->get_creation() ?>"><?php echo $board->node()->get_creation(DATE_TINY) ?></em> ;
		<i><?php echo $board->node()->get('views').' '.$board->localize('views') ?>.</i>
	</div>
<?php endif ?>
<br class="clear" />