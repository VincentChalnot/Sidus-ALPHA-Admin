<?php
//This is the declaration of the location of the the framework directory
define('REAL_PATH', rtrim(dirname(dirname($_SERVER['SCRIPT_FILENAME'])), '/').'/sidus-alpha/');
define('PROJECT_REAL_PATH', rtrim(dirname($_SERVER['SCRIPT_FILENAME']), '/').'/');

define('HTTP_PATH', 'http://'.$_SERVER['HTTP_HOST'].rtrim(dirname(dirname($_SERVER['SCRIPT_NAME'])),'/').'/sidus-alpha/');
define('PROJECT_HTTP_PATH', 'http://'.$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['SCRIPT_NAME']),'/').'/');

define('ICON_TINY', 16);
define('ICON_SMALL', 24);
define('ICON_NORMAL', 32);
define('ICON_BIG', 64);
define('ICON_HUGE', 128);
define('ICON_FULL', 160);

define('DEFAULT_NODE_ID',1);

//We need to extends the board class.
require_once REAL_PATH.'includes/class.proto_board.php';

class admin_board extends proto_board{

	public function __construct()
	{
		parent::__construct();
		if (!$this->user()->is_connected() && !isset($_GET['login'])) {
			$this->controller()->redirect($this->node()->link(array('login')));
		}
	}

	protected function load_controller(){
		require_once PROJECT_REAL_PATH.'includes/class.admin_controller.php';
		$this->controller=new admin_controller($this->config);
	}

	public function project_link($node){
		return PROJECT_HTTP_PATH.'../index.php?node_id='.$node->get('node_id');
	}

	public function list_all_childs($node){
		$str='<ul>';
		foreach($node->get_childs() as $child){
			$str.='<li>'.$child->button(ICON_TINY);
			$str.=$this->list_all_childs($child);
			$str.='</li>';
		}
		$str.='</ul>';
		return $str;
	}

}
