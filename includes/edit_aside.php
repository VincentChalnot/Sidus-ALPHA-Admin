<div class="preview"><?php echo $board->node()->get_html_thumb(ICON_HUGE) ?></div>

<?php echo $board->node()->action_button('back') ?>

<p class="section"><?php echo $board->localize('Actions') ?><p>
	
<?php echo $board->node()->action_button(array('edit'=>'move'), 'Move') ?>
<?php echo $board->node()->action_button(array('edit'=>'copy'), 'Copy') ?>
<?php if($board->node()->get_auth('delete')): ?>
	<?php echo $board->node()->action_button('delete') ?>
<?php endif ?>

<p class="section"><?php echo $board->localize('Permissions') ?></p>
<?php echo $board->node()->action_button(array('edit'=>'permissions'), 'Permissions') ?>

<?php $inherited=$board->node()->get_inherited_node() ?>
<?php if($inherited->get('node_id')!=$board->node()->get('node_id')): ?>
	<p class="comment">Inherited from</p>
	<?php echo $inherited->button() ?>
<?php else: ?>
	<p class="comment"><?php echo $board->localize('General permissions') ?></p>
	<a class="button inactive permission">
		<span class="thumb">
			<img src="<?php echo HTTP_PATH.$board->config()->get('icons_directory') ?>user.png" alt="[Move]" width="<?php echo ICON_SMALL ?>" height="<?php echo ICON_SMALL ?>" />
		</span>
		<?php echo $board->localize('Anonymous') ?><br/>
		
		<?php foreach(array('read','add','edit','delete') as $auth): ?>
			<?php echo $board->generate_icon($auth,ICON_TINY) ?>
			<?php if($board->node()->get('anonymous_'.$auth)): ?>
				<b class="green">✔</b>&nbsp;&nbsp;
			<?php else: ?>
				<b class="red">✘</b>&nbsp;&nbsp;
			<?php endif ?>
		<?php endforeach ?>
	</a>
	<?php foreach((array)$board->node()->get_permissions() as $perm): ?>
		
	<?php endforeach ?>
	<p class="comment">Owners</p>
	<p class="comment">Masters</p>
<?php endif ?>
