<div id="errors">
	<noscript><p>Javascript is disabled, you need to activate Javascript to use this application.</p></noscript>
	<?php foreach((array) $board->error()->get_all() as $value): ?>
		<p onclick="this.parentNode.removeChild(this)"><?php echo $value['user_message'] ?></p>
	<?php endforeach ?>
	<?php $board->error()->clear() ?>
</div>
