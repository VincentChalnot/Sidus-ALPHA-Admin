<?php

//Set error reporting on:
//error_reporting(E_ALL^E_DEPRECATED);
//ini_set('display_errors','true');
require_once 'includes/class.admin_board.php'; //Initialization of framework
$board=new admin_board();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" Content="text/html; charset=UTF-8" />
		<title>Administration<?php ($board->node()!=false?' - '.$board->node()->get('title', false):'') ?></title>
		<link rel="alternate" type="application/atom+xml" title="RSS" href="<?php echo $board->node()->link('feed') ?>" />
		<link rel="alternate" media="print" href="<?php echo $board->node()->link('print') ?>" />
		<link rel="icon" type="image/png" href="<?php $t=$board->node()->get_thumb(); echo $t['url'] ?>" />
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<?php
		if(!$board->node()->is_root()){
			echo '<link rel="prev" href="'.$board->node()->get_parent()->link().'">';
		}
		if(file_exists(PROJECT_REAL_PATH.'../css/common.css')){
			echo '<link href="'.PROJECT_HTTP_PATH.'../css/common.css" rel="stylesheet" type="text/css" />';
		}
		if(file_exists(PROJECT_REAL_PATH.'css/types/'.$board->node()->get('type_name').'.css')){
			echo '<link href="'.PROJECT_HTTP_PATH.'css/types/'.$board->node()->get('type_name').'.css" rel="stylesheet" type="text/css" />';
		}
		if(isset($_GET['print'])){
			echo '<link href="css/print.css" rel="stylesheet" type="text/css" />';
		}
		if(strripos($_SERVER['HTTP_USER_AGENT'], 'mobile')){
			echo '<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0;">';
		}
		?>
		<!--[if IE]>
		<link href="css/ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		<script type="text/javascript" src="<?php echo PROJECT_HTTP_PATH.'scripts/jquery-1.10.2.min.js' ?>"></script>
	</head>
	<body>
		<table class="board"><tbody>
				<tr id="main_menu"><td><?php include PROJECT_REAL_PATH.'includes/main_menu.php' ?></td></tr>
				<tr id="main_header"><td><?php include PROJECT_REAL_PATH.'includes/main_header.php' ?></td></tr>
				<tr id="application"><td><?php include $board->get_view_path() ?></td></tr>
				<tr id="main_footer"><td><?php include PROJECT_REAL_PATH.'includes/main_footer.php' ?></td></tr>
			</tbody></table>
			<?php include PROJECT_REAL_PATH.'includes/errors.php' ?>
	</body>
</html>
